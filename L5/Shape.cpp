#include "Shape.h"
#include <iostream>

Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

Shape::Shape()
{
	this->_name = "";
	this->_type = "";
}


void Shape::printDetails() const
{
	std::cout << "The name is: %s, the type is: %s", this->_name, this->_type;
}

std::string Shape::getType() const
{
	return this->_type;
}

std::string Shape::getName() const
{
	return this->_name;
}

