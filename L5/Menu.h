#pragma once
#include "Shape.h"
#include "Canvas.h"
#include <vector>

class Menu
{
public:

	Menu();
	~Menu();
	Canvas getCanvas();

	// more functions..

private:
	Canvas _canvas;
};


