#pragma once

class Point
{
public:
	Point();
	Point(double x, double y);
	Point(const Point& other);
	virtual ~Point();

	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);

	double getX() const;
	double getY() const;
	void setPoint(double x, double y);

	double distance(const Point& other) const;

private:
	double _x;
	double _y;

};
