#include "Rectangle.h"

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(name, type)
{
	this->_points[0] = Point(a.getX(), a.getY());
	this->_points[1] = Point(a.getX() + width, a.getY() + length);
}

myShapes::Rectangle::~Rectangle()
{
	this->_a.~Point();
	this->_length = 0;
	this->_width = 0;
}

double myShapes::Rectangle::getArea() const
{
	return this->_length * this->_width;
}

double myShapes::Rectangle::getPerimeter() const
{
	return (this->_length * 2) + (this->_width * 2);
}

void myShapes::Rectangle::move(const Point& other)
{
	this->_a.setPoint(other.getX(), other.getY());
}




























