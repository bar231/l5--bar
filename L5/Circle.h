#pragma once

#include "Shape.h"
#include "Point.h"
#include "Menu.h"
#include <iostream>

#define PI 3.14

class Circle : public Shape
{
public:
	Circle(const Point& center, double radius, const std::string& type, const std::string& name);
	~Circle();

	const Point& getCenter() const;
	double getRadius() const;

	virtual void draw(const Canvas& canvas) override;
	virtual void clearDraw(const Canvas& canvas) override;

	virtual double getArea() const override;
	virtual double getPerimeter() const override;
	virtual void move(const Point& other) override;

private:
	Point _center;
	double _radius;
	std::string _type;
	std::string _name;


	// override functions if need (virtual + pure virtual)

};

