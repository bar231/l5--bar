#include "Point.h"
#include <iostream>
#include "math.h"

Point::~Point()
{
	this->_x = 0;
	this->_y = 0;
}

Point::Point()
{
	this->_x = 0;
	this->_y = 0;
}

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

void Point::setPoint(double x, double y)
{
	this->_x = x;
	this->_y = y;
}


Point::~Point()
{
	this->_x = 0;
	this->_y = 0;
}


Point Point::operator+(const Point& other) const
{
	Point* n = new Point(this->_x + other._x, this->_y + other._y);
	return *n;
}

Point& Point::operator+=(const Point& other)
{
	Point* n = new Point(this->_x + other._x, this->_y + other._y);
	return *n;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other) const
{
	return sqrt(pow(other._x - this->_x, 2) + pow(other._y - this->_y, 2) * 1.0);
}



