#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(name, type)
{
	this->_a = Point(a);
	this->_b = Point(b);
	this->_c = Point(c);
}
